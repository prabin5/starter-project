import {Link} from "react-router-dom";

export const NotFound = () => {
  return (
    <div className="flex justify-center items-center text-center h-screen">
      <div className="">
        <h1 className="text-9xl font-bold">404</h1>
        <div>
          <p className="text-2xl font-semibold">Page not found</p>
          <Link to="/" className="text-2xl font-semibold underline text-info-500 block mt-4">
            Go back to home
          </Link>
        </div>
      </div>
    </div>
  );
};